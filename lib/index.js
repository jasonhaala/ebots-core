'use strict';

const controller = require('./coreBot.js');
const server = require('./server');
const FacebookApi = require('./platforms/facebook/api');
//const botlets = require('./botlets');
const dialogflowV2 = require('./services/dialogflow/v2');

module.exports = {

    controller: controller,

    server: server,

    //useBotlet: botlets,

    facebookApi: FacebookApi,

    dialogflowV2: dialogflowV2,

}
