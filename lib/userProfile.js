'use strict';

/*
 Placeholder for the retrieved user profile object
 */
let userProfile;

let channelUserId = null;

/*
 Functionality allows to return only a specific field instead of the entire profile
 */
let requestedField;

/*
 Default value to determine if the user profile should be updated after it's retrieved
 */
let updateCache = false;
let updateDatabase = false;

/*
 Determine if this is a new user that hasn't connected with the bot before
 Assume this is a new user until their profile is found in our system.
 */
let isNewUser = false;


module.exports = function(controller) {

    /**
     * @param {Object|String|Number} messageOrUserId - You can pass the bot object or the channel user id.
     * @param {Function} callback
     */
    return function(messageOrUserId, callback) {

        let requestedField = null;

        getUserProfile(messageOrUserId, requestedField, callback);
    }

    function getUserProfile(messageOrUserId, field, callback) {

        requestedField = field;

        /*
         The user id must be set before continuing additional checks
         */
        if (typeof messageOrUserId === 'string' || typeof messageOrUserId === 'number') {
            channelUserId = messageOrUserId;
        } else if (typeof messageOrUserId === 'object' && messageOrUserId.sender.id) {
            channelUserId = messageOrUserId.sender.id;
        }

        if (! channelUserId) {
            controller.log.debug('Unable to locate user profile. The channel user id is missing.');

            return returnProfile(callback);
        }

        console.log(`Locating profile for userId: ${channelUserId}...`);

        /*
         Check if the profile is cached
         */
        controller.cache.get(channelUserId, function(error, result) {
            if (error) {
                console.error(error);
            } else if (result) {
                let parsedResult = JSON.parse(result);
                if (parsedResult.userId) {
                    userProfile = parsedResult;
                    console.log('Returning user profile from cache.');

                    return returnProfile(callback);
                }
            }

            // The user's profile wasn't found in the cache. Cache the new profile, if retrieved.
            updateCache = true;

            /*
             Check the database
             */
            controller.ebots.db.users.get(channelUserId, function(error, user) {
                if (error) {
                    controller.log.error(error);

                    return returnProfile(callback);
                } else if (user) {
                    console.log('Returning user profile from database.');
                    userProfile = user;

                    return returnProfile(callback);
                }

                // User's profile wasn't found in our system
                isNewUser = true;

                /*
                 Request the new user's profile from the channel
                 */
                checkChannel(channelUserId, function(error, channelProfile) {
                    if (error) {
                        controller.log.error(error);
                    } else if (channelProfile) {
                        controller.log.debug('Returning user profile from channel.');

                        userProfile = channelProfile;

                        // Update this profile in the database
                        updateDatabase = true;
                    }

                    return returnProfile(callback);
                });
            });
        });
    }


    function checkChannel(channelUserId, callback) {
        /*
         Request the user's profile from the connected channel
         Our custom API services will return profile data already formatted to the bot's users schema model
         */
        let apiService;
        let channel = controller.ebots.channel.name;
        switch (channel) {
            case 'facebook':
                apiService = require('./platforms/facebook/api');
        }

        if (apiService) {
            apiService.requestUserProfile(controller, channelUserId, function(error, channelProfile) {
                callback(error, channelProfile);
            });
        } else {
            // Invalid channel
            callback(`Unable to request user profile. Invalid channel: ${channel}`);
        }
    }


    function returnProfile(callback) {
        if (! userProfile) {
            return callback({}, isNewUser);
        } else {
            if (updateDatabase) {
                /*
                 Save the profile to the database
                 Will also be cached
                 */
                storeProfile(channelUserId, userProfile);
            } else if (updateCache && channelUserId) {
                /*
                 Cache the profile
                 */
                cacheProfile(channelUserId, userProfile);
            }

            // Set the initial profile value to be returned
            let returnData = userProfile;

            if (requestedField) {
                if (returnData[requestedField]) {
                    // Return only the requested profile field
                    returnData = returnData[requestedField];
                } else {
                    // No matching field found
                    returnData = null;
                }
            }

            return callback(returnData, isNewUser);
        }
    }

    function cacheProfile(channelUserId, userProfile) {
        console.log('Caching profile...');
        let cacheTTL = controller.ebots.config.cache.ttl;
        controller.cache.set(channelUserId, JSON.stringify(userProfile), 'EX', cacheTTL);
        console.log('Profile cached');
    }

    function storeProfile(channelUserId, userProfile) {
        controller.log.debug('Updating/saving user profile...')
        controller.ebots.db.users.save(userProfile, function(error) {
            if (error) {
                // @todo handle error. Send to queue to re-attempt save?
                controller.log.error(error);
                // Send alert
            } else {
                cacheProfile(channelUserId, userProfile)
            }
        });
    }

}
