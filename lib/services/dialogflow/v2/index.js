const debug = require('debug')('dialogflow-middleware');
const util = require('./util');
const v2Api = require('./api');
const path = require('path');

module.exports = {

    process: async function(message, config, callback) {

        if (message.lang) {
            config.lang = message.lang;
        }

        config = checkConfig(config);

        const dialogflowApi = v2Api(config);
        if (! dialogflowApi) {

            return callback('Unable to load Dialog');
        }

        const sessionId = util.generateSessionId(config, message);

        try {
            // Send the request
            const response = await dialogflowApi.query(sessionId, config.lang, message.text);
            //console.log(response);
            //console.log('Dialogflow response')
            //console.log('=============================')
            // Attach the formatted NLP message to the original message
            Object.assign(message, response);
            //console.log('Dialogflow annotated message: %O', message);

            return callback(null, message);
        } catch (error) {
            console.error('Dialogflow returned error', error);

            return callback(error);
        }
    },

    /**
     *
     * @param {Object} config
     * @returns {{}}
     */
    middleware: function(config = {}) {
        if (! config.ignoreType) {
            config.ignoreType = 'self_message';
        }

        config = checkConfig(config);

        const ignoreTypePatterns = util.makeArrayOfRegex(config.ignoreType || []);
        const middleware = {};

        const dialogflowApi = middleware.api = api(config);
        if (! dialogflowApi) {
            throw new Error('Dialogflow failed to load.');

            process.exit();
        }

        middleware.receive = async function(bot, message, next) {
            if (!message.text || message.is_echo || message.type === 'self_message') {

                return next();
            }

            for (const pattern of ignoreTypePatterns) {
                if (pattern.test(message.type)) {
                    debug('skipping call to Dialogflow since type matched ', pattern);

                    return next();
                }
            }

            const sessionId = util.generateSessionId(config, message);
            const lang = message.lang || config.lang;

            debug(`Sending message to dialogflow. sessionId=${sessionId}, language=${lang}, text=${message.text}`);

            try {
                const response = await dialogflowApi.query(sessionId, lang, message.text);
                Object.assign(message, response);
                debug('dialogflow annotated message: %O', message);

                next();
            } catch (error) {
                debug('dialogflow returned error', error);

                next(error);
            }
        };

        middleware.hears = function(patterns, message) {
            const regexPatterns = util.makeArrayOfRegex(patterns);

            for (const pattern of regexPatterns) {
                if (pattern.test(message.intent) && message.confidence >= config.minimumConfidence) {
                    debug('dialogflow intent matched hear pattern', message.intent, pattern);

                    return true;
                }
            }

            return false;
        };

        middleware.action = function(patterns, message) {
            const regexPatterns = util.makeArrayOfRegex(patterns);

            for (const pattern of regexPatterns) {
                if (pattern.test(message.action) && message.confidence >= config.minimumConfidence) {
                    debug('Dialogflow action matched hear pattern', message.intent, pattern);

                    return true;
                }
            }

            return false;
        };

        return middleware;
    }

};


/**
 * Checks the provided config and sets and necessary default settings
 *
 * @param {object} config - the provided configuration settings
 * @return {object} validated configuration with defaults applied
 */
function checkConfig(config = {}) {
    if (! config.sessionIdProps) {
        config.sessionIdProps = ['user', 'channel'];
    }
    if (! config.minimumConfidence) {
        config.minimumConfidence = 0.5;
    }
    if (! config.lang) {
        config.lang = 'en';
    }

    if (! config.keyFilename) {
        throw new Error('Dialogflow keyFilename must be provided for v2.');

        return null;
    }

    return config;
}
