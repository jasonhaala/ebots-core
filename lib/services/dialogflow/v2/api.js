const debug = require('debug')('dialogflow-middleware');
const dialogflow = require('dialogflow');
const structProtoToJson = require('./structjson').structProtoToJson;
const lodash = require('lodash');

/*
Message Factories
 */
const factories = {
    facebook: require('ebots-facebook-factory')
}


module.exports = function(options) {

    return new DialogFlowApi(options)

};

class DialogFlowApi {
    constructor(options) {
        this.options = options;

        const opts = {
            keyFilename: options.keyFilename,
        };

        if (options.projectId) {
            opts.projectId = options.projectId;
        } else {
            try {
                const keyFile = require(options.keyFilename);
                this.projectId = keyFile.project_id;
            } catch (error) {
                throw new Error('projectId must be provided or available in the keyFile.');
            }
        }
        if (options.credentials) {
            opts.credentials = options.credentials;
        }
        if (options.email) {
            opts.email = options.email;
        }
        if (options.port) {
            opts.port = options.port;
        }
        if (options.promise) {
            opts.promise = options.promise;
        }
        if (opts.servicePath) {
            opts.servicePath = options.servicePath;
        }

        this.app = new dialogflow.SessionsClient(opts);
    }

    query(sessionId, languageCode, text) {
        const request = {
            session: this.app.sessionPath(this.projectId, sessionId),
            queryInput: {
                text: {
                    text: text,
                    languageCode: languageCode,
                },
            },
        };

        /*
        Send a Dialogflow request to detect the intent
         */
        return new Promise((resolve, reject) => {
            this.app.detectIntent(request, function(error, response) {
                if (error) {
                    debug('dialogflow api error: ', error);

                    reject(error);
                } else {
                    debug('dialogflow api response: ', response);
                    formatFulfillmentMessages(response, function(error, formattedMessages) {
                        const normalizedReponse = DialogFlowApi.normalizeResponse(response, formattedMessages);
                        debug('Dialogflow normalized data:', normalizedReponse);

                        resolve(normalizedReponse);
                    });
                }
            });
        });
    }

    // return standardized format
    static normalizeResponse(response, formattedMessages) {
        return {
            intent: lodash.get(response, 'queryResult.intent.displayName', null),
            entities: structProtoToJson(response.queryResult.parameters),
            action: lodash.get(response, 'queryResult.action', null),
            fulfillment: {
                text: lodash.get(response, 'queryResult.fulfillmentText', null),
                messages: formattedMessages,
            },
            confidence: lodash.get(response, 'queryResult.intentDetectionConfidence', null),
            nlp: response,
            // contexts,
            // parameters
        };
    }
}


/**
 * https://dialogflow.com/docs/reference/message-objects
 *
 * @param {Object} response - The response received from Dialogflow
 * @param {Function} callback
 * @returns {Array}
 */
function formatFulfillmentMessages(response, callback) {
    let formattedMessages = [];

    let messages = lodash.get(response, 'queryResult.fulfillmentMessages', null);
    if (! messages) {

        return callback(null, formattedMessages);
    }

    let msgQty = messages.length
    for (let i = 0; i < messages.length; i ++) {
        let msg = messages[i];

        // Check for a channel-specific message & ensure that its factory is loaded
        let channel = msg.platform ? msg.platform.toLowerCase() : null;
        let factory = factories[channel] || null;

        // If a channel's factory is not loaded, skip that message and send an alert.
        // Plain text messages and JSON payloads don't require a factory
        if (! factory && ! (msg.text || msg.payload)) {
            console.error(`Dialogflow error: A ${channel} factory was not loaded. Skipping message: `, msg);

            continue;
        }

        /*
         Check universal message types
         */
        if (msg.text) {
            /*
             Text response
             */
            // Select a random message
            let textMessages = msg.text.text;
            let textMsg = textMessages[Math.floor(Math.random() * textMessages.length)];

            formattedMessages.push(textMsg);
        } else if (msg.payload) {
            /*
             Returns a response containing a custom, platform-specific payload.

             See the Intent.Message.Platform type for a description of the
             structure that may be required for your platform
             */

            formattedMessages.push(msg.payload);
        } else if (msg.image) {
            /*
             Image response
             */
            let imageMsg = new factory.Message()
            .attachment(
                new factory.Image().url(msg.image.imageUri)
            );

            formattedMessages.push(imageMsg);
        } else if (msg.quickReplies) {
            /*
             Quick replies response
             */

            // Create the quick reply buttons
            let quickReplies = [];
            for (let q = 0; q < msg.quickReplies.quickReplies.length; q++) {
                let quickReplyText = msg.quickReplies.quickReplies[q];
                let quickReply = factory.QuickReply.text(quickReplyText, quickReplyText);
                quickReplies.push(quickReply);
            }

            // Create the message
            let quickReplyMsg = new factory.Message()
            .text(msg.quickReplies.title)
            .quickReplies(quickReplies);

            formattedMessages.push(quickReplyMsg);
        } else if (msg.card) {
            /*
             Card response
             */

            // Add attachment element
            let cardElement = new factory.GenericCard().title(msg.card.title);
            if (msg.card.subtitle) {
                cardElement.subtitle(msg.card.subtitle);
            }
            if (msg.card.imageUri) {
                cardElement.image(msg.card.imageUri);
            }

            // Add the buttons
            let cardButtons = [];
            for (let b = 0; b < msg.card.buttons.length; b++) {
                let cardButton;
                let btn = msg.card.buttons[b];

                let lowerBtnValue = btn.postback.toLowerCase();
                if (lowerBtnValue.startsWith('http')) {
                    // Url Button
                    cardButton = factory.Button.openUrl(btn.postback, btn.text);
                } else {
                    // Postback Button
                    cardButton = factory.Button.postback(btn.text, btn.postback)
                }

                cardButtons.push(cardButton);
            }

            cardElement.buttons = cardButtons;

            let cardMsg = new factory.Message()
            .attachment(new factory.GenericTemplate('square')
                .elements([
                    cardElement
                ])
            );

            formattedMessages.push(cardMsg);
        } else if (channel === 'actions_on_google') {
            // Actions on Google is currently not supported
            /*if (msg.basicCard) {
             /!*
             Basic card response for Actions on Google
             *!/
             } else if (msg.carouselSelect) {
             /!*
             Carousel card response for Actions on Google
             *!/
             } else if (msg.listSelect) {
             /!*
             List card response for Actions on Google
             *!/
             } else if (msg.suggestions) {
             /!*
             Suggestion chips for Actions on Google
             *!/
             } else if (msg.linkOutSuggestion) {
             /!*
             Link out suggestion chip for Actions on Google
             *!/
             } else if (msg.simpleResponses) {
             /!*
             The voice and text-only responses for Actions on Google
             *!/
             }*/
        } else {
            // Does not match any set message type
            console.error('Dialogflow error. No matching message type was found. Skipping message: ', msg);
        }
    }

    return callback(null, formattedMessages);
}
