'use strict';

const express = require('express');
const router = express.Router();
const request = require('request');
const Bot = require('../../storage/mongodb/models/bot').model;

module.exports = (controller) => {

    const appConfig = controller.ebots.config.app;

    /**
     * Chatbase - Domain verification to enable url click tracking
     *
     * https://chatbase.com/documentation/taps
     */
    router.get('/chatbase/verify', (req, res) => {
        let redirectUrl = encodeURI(`https://${appConfig.url}/chatbase/response&platform`);
        let params = `?api_key=${appConfig.chatbase.apiKey}url=${redirectUrl}=${controller.ebots.channel.name}`;

        request.post('https://chatbase.com/api/click?='+params, function(err, res, body) {
            if (err) {
                console.log('Could not subscribe to page messages!');
                throw new Error(err);
            } else {
                console.log('Successfully subscribed to Facebook events:', body);
                controller.startTicking();
            }
        });
    });


    /**
     * Facebook message events forwarded from the eBots router
     */
    router.post('/api/incoming-messages', (req, res) => {
        // Internal parameters
        const AppApiToken = appConfig.apiToken;
        const activeChannel = controller.ebots.channel.name;

        // Expected parameters
        //console.log(req.query);
        const body = req.body || null;
        //console.log('Request Body:');
        //console.log(JSON.stringify(body));

        const requestApiToken = req.query.apiToken || null;

        let channelData = req.query.channel;
        if (typeof channelData === 'string') {
            channelData = JSON.parse(channelData);
        }
        const requestChannel = channelData.name || null;

        let botData = req.query.bot;
        if (typeof botData === 'string') {
            botData = JSON.parse(botData);
        }
        const botId = botData.id || null;

        let senderData = req.query.sender;
        if (typeof senderData === 'string') {
            senderData = JSON.parse(senderData);
        }
        const senderId = senderData.id || null;

        const msgType = req.query.msgType || null;

        /**
         * Validate the request
         */
        if (! AppApiToken) {
            console.error(`Incoming webhook message rejected. Missing API token. Check configs.`);

            return res.status(503).send('Token unavailable');
        } else if (! activeChannel) {
            console.error(`Incoming webhook message rejected. Missing active channel platform. Check configs.`);

            return res.status(400).send('Bot not active on this channel.');
        } else if (! requestApiToken) {
            console.error(`Incoming webhook message rejected. Missing API token. Check configs.`);

            return res.status(400).send('Missing token');
        } else if (requestApiToken !== appConfig.apiToken) {
            console.error(`Incoming webhook message rejected. API token mismatch.`);

            return res.status(400).send('Token mismatch');
        } else if (! botId) {
            console.error(`Incoming webhook message rejected. Missing Bot Id.`);

            return res.status(400).send('Missing bot');
        } else if (! requestChannel) {
            console.error(`Incoming webhook message rejected. Missing channel name.`);

            return res.status(400).send('Missing channel name');
        } else if (! senderId) {
            console.error(`Incoming webhook message rejected. Missing sender id.`);

            return res.status(400).send('Missing sender');
        }

        if (activeChannel.toLowerCase() !== requestChannel.toLowerCase()) {
            console.error(`Incoming webhook message rejected. Invalid channel.`);

            return res.status(400).send('Invalid channel');
        }

        /**
         * Valid message received.
         */
        // Send a response to the router that the webhook has been successfully received.
        res.status(200).send('OK');

        // Spawn a new bot to handle the message event
        let bot = controller.spawn({});

        controller.handleWebhookPayload(req, res, bot);
    });


    /**
     * Facebook Webhook Verification
     *
     * This should only be used if accepting message events directly from Facebook.
     * If using a message router, webhook verification will be handled there.
     */
    router.get('/facebook/messages', function(req, res) {
        const internalVerifyToken = config.facebook.verifyToken;
        if (! internalVerifyToken) {
            // Missing verify token
            console.error(`Facebook webhook validation failed. Verify tokens is missing. Check config.`);

            return res.status(503).send('Unavailable');
        }

        // Parse the received query params
        const mode = req.query['hub.mode'];
        const verifyToken = req.query['hub.verify_token'];
        const challenge = req.query['hub.challenge'];

        // Check if all required parameters are set in order to process the request
        if (verifyToken && challenge && mode && mode === 'subscribe') {
            // Check if the verify tokens match
            if (verifyToken === internalVerifyToken) {
                // Respond with the challenge token from the request
                console.log('Facebook webhook successfully verified.');

                return res.status(200).send(challenge);
            } else {
                // Verify tokens do not match
                console.log(`Facebook webhook validation failed. Verify tokens did not match.`);

                return res.sendStatus(400);
            }
        } else {
            // Invalid subscribe request
            return res.sendStatus(403);
        }
    });


    /**
     * Handle message events received directly from Facebook
     *
     * This should only be used if accepting message events directly from Facebook.
     * If using a message router, webhook verification will be handled there.
     */
    router.post('/facebook/messages', function(req, res) {

        // @TODO we should enforce the token check here

        let body = req.body;

        //console.log('Request Body:');
        //console.log(JSON.stringify(body));

        // Check that this is an event from a page subscription
        if (body.object === 'page') {
            // Send a success response back to Facebook
            // Facebook must receive a 200 response within 20 seconds of the request.
            res.status(200).send('EVENT_RECEIVED');

            // @todo It would make sense here to store the received message until it's been processed on our end.

            // Iterate over each entry - there may be multiple if batched
            body.entry.forEach(function(entry) {

                // Set the message.
                // entry.messaging is an array, but will only ever contain one message.
                let message = entry.messaging[0];
                //console.log('Facebook Webhook Message:');
                //console.log(JSON.stringify(message));

                // Get the sender and page details
                const pageId = message.recipient.id;
                const senderId = message.sender.id;
                //const msgId = message.message.mid;
                //const timestamp = message.timestamp;
                //console.log('Page id: '+pageId);
                //console.log('Sender id: '+senderId);

                // Get the bot API url to Transfer the incoming message.
                Bot.findOne({'facebook.pageId': pageId}, function(error, result) {
                    if (error) {
                        console.log(`Error retrieving Facebook bot (${pageId}) during webhook event.`);
                        console.error(error);
                    } else if (result) {
                        console.log(result);
                        const clientTag = result.clientTag;
                        if (! clientTag) {
                            console.error(`Missing bot tag for Facebook page: (${pageId})`);
                        } else {
                            // Spawn a new bot to handle the message event
                            let bot = controller.spawn({});

                            controller.handleWebhookPayload(req, res, bot);
                        }
                    } else {
                        // No matching bot was found
                        console.error(`Unable to find Facebook bot (${pageId}) during webhook event.`);
                    }
                });
            });
        } else {
            // Returns a 400 error if event is not from a page subscription
            console.log(`Missing or invalid event.`);

            return res.sendStatus(400);
        }
    });


    return router;
};
