'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const helmet = require('helmet');

/**
 * @param {Object} controller // The bot controller
 */
module.exports = function(controller, callback) {
    let appConfig = controller.ebots.config.app;
    if (! appConfig.port) {
        throw new Error('Unable to launch server. Required server port is missing.');

        process.exit(1);
    }

    // // Determine which view engine to use. Defaults to: EJS
    let viewEngine = appConfig.viewEngine || 'ejs';

    const server = express();
    server.set('port', appConfig.port);
    server.set('view engine', viewEngine);
    server.use(helmet()); // Helps protect against known web vulnerabilities by setting HTTP headers appropriately
    server.use(bodyParser.json());  // Process application/json
    server.use(bodyParser.urlencoded({ extended: true }));  // Process application/x-www-form-urlencoded
    server.use(express.static('public'));

    /**
     * Spin up the server
     */
    server.listen(appConfig.port, null, function() {
        if (! isLocalEnv) {
            controller.trigger('server_up', [server]);
        } else {
            const commandLineArgs = require('command-line-args');
            const ops = commandLineArgs([
                {
                    name: 'lt',
                    alias: 'l',
                    args: 1,
                    description: 'Use localtunnel.me to make your bot available on the web.',
                    type: Boolean,
                    defaultValue: false},
                {
                    name: 'ltsubdomain',
                    alias: 's',
                    args: 1,
                    description: 'Custom subdomain for the localtunnel.me URL. This option can only be used together with --lt.',
                    type: String,
                    defaultValue: null
                },
            ]);

            if (ops.lt) {
                /**
                 * Build a local tunnel for testing on local environments
                 */
                const localtunnel = require('localtunnel');

                let tunnel = localtunnel(server.settings.port, {subdomain: ops.ltsubdomain}, function(err, tunnel) {
                    if (err) {
                        new Error(err);

                        process.exit();
                    }

                    controller.trigger('server_up', [server, tunnel.url]);
                });

                tunnel.on('close', function() {
                    console.log(`The server tunnel connection has closed.`);

                    process.exit();
                });
            } else {
                if (ops.ltsubdomain !== null) {
                    new Error('--ltsubdomain can only be used together with --lt.');

                    process.exit();
                } else {
                    controller.trigger('server_up', [server]);
                }
            }
        }
    });

    /**
     * Set the pre-defined application routes
     */
    let baseRoutes = require('./routes')(controller);
    server.use('/', baseRoutes);


    /**
     * Import all middleware present in the /middleware directory
     */
    let middlewarePath = path.join(__dirname, '../middleware');
    fs.readdirSync(middlewarePath).forEach(function(file) {
        if (file !== 'disabled') {
            require(middlewarePath +'/'+ file)(controller);
        }
    });


    /**
     * Initialize the core dialogs & events for the bot
     */
    let eventsPath = path.join(__dirname, '../events/core');
    fs.readdirSync(eventsPath).forEach(function(file) {
        if (file !== 'disabled') {
            require(eventsPath +'/'+ file)(controller);
        }
    });


    /*
     Add the express server to the controller.
     */
    // Remove until necessary to reduce overhead.
    //controller.webserver = server;


    callback(null, server)
}
