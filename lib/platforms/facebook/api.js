'use strict';

const request = require('request');

var apiVersion = 'v2.11';
var apiHost = 'graph.facebook.com';

module.exports = {

    /*
     user_profile: {}
     messenger_profile: {
     target_audience: [Function: target_audience],
     delete_target_audience: [Function: delete_target_audience],
     get_target_audience: [Function: get_target_audience],
     postAPI: [Function: postAPI],
     deleteAPI: [Function: deleteAPI],
     getAPI: [Function: getAPI],
     get_messenger_code: [Function: get_messenger_code]
     },
     broadcast: {
     create_message_creative: [Function: create_message_creative],
     remove_user_from_label: [Function: remove_user_from_label],
     get_labels_by_user: [Function: get_labels_by_user],
     get_label_details: [Function: get_label_details],
     get_all_labels: [Function: get_all_labels],
     remove_label: [Function: remove_label],
     cancel_scheduled_broadcast: [Function: cancel_scheduled_broadcast],
     get_broadcast_status: [Function: get_broadcast_status]
     },
     insights: { get_insights: [Function: get_insights] } },
     attachment_upload: { upload: [Function: upload] },
     nlp:
     { enable: [Function: enable],
     disable: [Function: disable],
     postAPI: [Function: postAPI] },
     tags: { get_all: [Function: get_all] },
     handover:
     { get_secondary_receivers_list: [Function: get_secondary_receivers_list],
     take_thread_control: [Function: take_thread_control],
     pass_thread_control: [Function: pass_thread_control],
     request_thread_control: [Function: request_thread_control],
     get_thread_owner: [Function: get_thread_owner] },
     */

    requestUserProfile: (controller, facebookUserId, callback) => {
        controller.api.user_profile(facebookUserId).then(function(channelProfile) {

            // normalize this to match the database schema used by the bot
            // @todo Temporarily using hard-coded values here. Create functionality in the user model that will return a formatted model object.
            let user = {
                _id: channelProfile._id,
                userId: facebookUserId,
                firstName: channelProfile.first_name,
                lastName: channelProfile.last_name,
                email: channelProfile.email || null,
                picUrl: channelProfile.picture.data.url || null,
                gender: channelProfile.gender || null, // Requires pages_user_gender permission
                locale: channelProfile.locale || null, // Requires pages_user_locale permission
                timezone_offset: channelProfile.timezone || null, // Requires pages_user_timezone permission
            };

            callback(null, user);

        }).catch(function(err) {
            callback(err);
        });
    },

    /**
     * Send a broadcast message to all connected users.
     * On success, a numeric broadcast_id will be returned that can be used to identify the broadcast for analytics purposes
     *
     * @param controller
     * @param {object} message // Message object with:
     *      {string} message_creative_id
     *      {string} notification_type: '<REGULAR | SILENT_PUSH | NO_PUSH>'
     *      {string} tag
     */
    sendBroadcastMessage: (controller, message) => {
        controller.api.broadcast.send(message, null, function (err, body) {
            // Your awesome code here
            console.log('Broadcast Id:');
            console.log(body['broadcast_id']);
            // And here
        });
    },

    /**
     * By default, the Broadcast API sends your message to all open conversations with your Messenger bot.
     * To allow you broadcast to a subset of conversations, the Broadcast API supports 'custom labels',
     * which can be associated with individual PSIDs.
     *
     * @param controller
     * @param {string} labelName
     */
    sendTargetedBroadcastMessage: (controller, labelName) => {
        controller.api.broadcast.create_label(labelName, function (err, body) {

        });
    },

    /**
     * Once a broadcast has been delivered, you can find out the total number of people it reached
     *
     * @param controller
     * @param {string} broadcastId
     */
    getBroadcastMetrics: (controller) => {
        controller.api.broadcast.get_broadcast_metrics(broadcastId, function (err, body) {

        });
    },

    /**
     *
     * @param controller
     * @param labelName
     */
    createLabel: (controller, labelName) => {
        controller.api.broadcast.create_label(labelName, function (err, body) {

        });
    },

    addLabelToUser: (controller, message, labelId) => {
        controller.api.broadcast.add_user_to_label(message.user, "<LABEL_ID>", function (err, body) {
            // Your awesome code here
        });
    },

    /**
     * Set the greeting message to display on welcome screen
     *
     * https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/greeting
     *
     * You can personalize the greeting text using the person's name. You can use the following template strings:
     * {{user_first_name}}
     * {{user_last_name}}
     * {{user_full_name}}
     *
     * @param {array|string} greetingText // Greeting text
     */
    setGreeting: (controller, greeting) => {

        // Convert plain text greetings into the required array, setting it as the default locale
        if (typeof greeting === 'string') {
            greeting = [{
                locale: "default",
                text: greeting
            }];
        }
        console.log(`Setting Facebook Greeting text...`);
        controller.api.messenger_profile.greeting(greeting);
    },


    /**
     * Remove the greeting message.
     */
    deleteGreeting: (controller) => {
        controller.api.messenger_profile.delete_greeting();
    },

    /**
     * Get the greeting message.
     */
    getGreeting: (controller) => {
        controller.api.messenger_profile.get_greeting();
    },

    /**
     * Set the payload value of the "Get Started" button
     *
     * @param {string} payload // Button payload
     */
    setGetStartedButton: (controller, payload) => {
        console.log(`Setting Facebook "Get Started" button...`);
        controller.api.messenger_profile.get_started(payload);
    },

    /**
     * Clear the payload value of the "Get Started" button and remove it.
     */
    deleteGetStartedButton: (controller) => {
        controller.api.messenger_profile.delete_get_started();
    },

    /**
     * Get the "Get Started" payload value.
     */
    getGetStartedButton: (controller) => {
        controller.api.messenger_profile.get_get_started();
    },

    /**
     * Create a persistent menu for the Bot
     *
     * @param {array} menuItems// menu_item objects
     */
    setPersistentMenu: (controller, menuItems) => {
        console.log(`Setting Facebook persistent menu...`);
        controller.api.messenger_profile.menu(menuItems);
    },

    /**
     * Clear the persistent menu setting
     */
    deletePersistentMenu: (controller) => {
        controller.api.messenger_profile.delete_menu();
    },

    /**
     * Get the persistent menu items.
     */
    getPersistentMenu: (controller) => {
        controller.api.messenger_profile.get_menu();
    },

    /**
     * Set the account link
     * @param {string} accountLink // the account link
     */
    setAccountLinkingUrl: (controller, accountLink) => {
        console.log(`Setting Facebook Account Linking URL...`);
        controller.api.messenger_profile.account_linking(accountLink);
    },

    /**
     * Remove the account link
     */
    deleteAccountLinkingUrl: (controller) => {
        controller.api.messenger_profile.delete_account_linking();
    },

    /**
     * Set whitelisted domain(s)
     *
     * All domains must be valid and use https.
     * Up to 10 domains allowed.
     *
     * @param {string|array} domains // A single domain or a list of domains to whitelist.
     */
    setWhitelistedDomains: (controller, domains) => {
        let maxDomainsAllowed = 10;

        if (typeof domain === 'string') {
            // Convert string to array
            domains = [domains];
        } else {
            const whitelistDomainCount = domains.length;
            if (whitelistDomainCount > maxDomainsAllowed) {
                console.log(`Attempted to whitelist too many domains for Facebook. Reduced to maximum (${maxDomainsAllowed}. Check configs.`)
                domains.slice(maxDomainsAllowed - 1);
            }
        }
        console.log(`Setting Facebook whitelisted domains...`);
        controller.api.messenger_profile.domain_whitelist(domains);
    },

    /**
     * Remove all domains from the whitelist
     */
    deleteWhitelistedDomains: (controller) => {
        controller.api.messenger_profile.delete_domain_whitelist();
    },

    /**
     * Get a list of the whitelisted domains.
     */
    getWhitelistedDomains: (controller) => {
        controller.api.messenger_profile.get_domain_whitelist();
    },

    /**
     * Add the home_url setting
     *
     * @param {object} homeUrl // A home_url object with the properties url, webview_height_ratio, in_test
     */
    setHomeUrl: (controller) => {
        console.log(`Setting Facebook Home URL...`);
        controller.api.messenger_profile.home_url(homeUrl);
    },

    /**
     * Remove the home_url setting
     */
    deleteHomeUrl: (controller) => {
        controller.api.messenger_profile.delete_home_url();
    },

    /**
     * Get the home_url
     */
    getHomeUrl: (controller) => {
        controller.api.messenger_profile.get_home_url();
    },

    /**
     * @param {object} paymentSettings // A JSON object with the properties privacy_url, public_key, testers
     */
    setPaymentSettings: (controller, settings) => {
        console.log(`Setting Facebook payments options...`);
        controller.api.messenger_profile.payment_settings(settings);
    },

    /**
     * Remove the payment_settings setting
     */
    deletePaymentSettings: (controller) => {
        controller.api.messenger_profile.delete_payment_settings();
    },

    /**
     * Get the payment_settings property of your bot's Messenger Profile
     */
    getPaymentSettings: (controller) => {
        controller.api.messenger_profile.get_payment_settings();
    },

    subscribeApp: (controller) => {
        request.post('https://graph.facebook.com/me/subscribed_apps?access_token=' + controller.config.access_token,
            function(err, res, body) {
                if (err) {
                    applog.error('Could not subscribe to page messages!');
                    throw new Error(err);
                } else {
                    applog.debug('Successfully subscribed to Facebook events:', body);
                    controller.startTicking();
                }
            });
    },

    /**
     * Get the Facebook App Id used by the bot
     */
    getAppId: (controller) => {
        request.get('https://graph.facebook.com/app/?access_token='+ controller.config.access_token,
            function(err, res, body) {
                if (err) {
                    applog.debug(`Could not get Facebook Page Id! Check your page acccess token.`);
                    throw new Error(err);
                } else {
                    let json = null;
                    try {
                        json = JSON.parse(body);
                    } catch(err) {
                        applog.debug('Error parsing JSON response from Facebook');
                        throw new Error(err);
                    }
                    if (json.error) {
                        throw new Error(json.error.message);
                    } else {
                        controller.config.app = json;

                        request.get('https://graph.facebook.com/me?access_token='+ controller.config.access_token,
                            function(err, res, body) {
                                if (err) {
                                    applog.warning(`Could not get Facebook Page Id! Check your page acccess token.`);
                                    throw new Error(err);
                                } else {
                                    var json = null;
                                    try {
                                        json = JSON.parse(body);
                                    } catch(err) {
                                        applog.error('Error parsing JSON response from Facebook');
                                        throw new Error(err);
                                    }
                                    if (json.error) {
                                        throw new Error(json.error.message);
                                    } else {
                                        controller.config.page = json;
                                    }
                                }
                            });


                    }
                }
            });
    },

    /**
     * Attachment Upload
     *
     * Uploads an attachment to Facebook's servers, returning a reusable Attachment Id.
     *
     * @param controller
     * @param {Object} attachment - use factory.Image, factory.Video, etc.
     */
    attachmentUpload: (controller, attachment, callback) => {
        controller.api.attachment_upload.upload(attachment, function (error, attachmentId) {
            if(error) {
                callback(error);
            } else {
                callback(null, attachmentId);
            }
        });
    },

    directSend: (controller, message, recipientId) => {
        let postData = {
            recipient: {
                id: recipientId
            },
            message: message
        }

        request({
                method: 'POST',
                json: true,
                headers: {
                    'content-type': 'application/json',
                },
                body: postData,
                uri: 'https://'+ apiHost +'/'+ apiVersion +'/me/messages?access_token='+controller.ebots.config.facebook.pageAccessToken
            },
            function(error, res, body) {
                if (error) {
                    console.log(error)
                }

                if (body.error) {
                    console.log(body.error)
                }

                console.log('WEBHOOK SUCCESS', body);
            });
    }

}
