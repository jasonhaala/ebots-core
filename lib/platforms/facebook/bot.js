'use strict';

const fs = require('fs');
const Botkit = require('botkit');
const path = require('path');
const messageFactory = require('ebots-facebook-factory');

/**
 *
 * @param {object} customControls // The bot's custom configuration data
 * @param {object} botkitOptions // Botkit configs
 * @param {boolean} bypassFacebookWebhook // Is a separate router acting as the Facebook message webhook
 */
module.exports = (customControls, botkitOptions = {}, bypassFacebookWebhook = false) => {
    /*
     Validate Required Parameters
     */
    const fbConfigs = customControls.config.facebook;

    if (! customControls.bot.id) {
        throw Error('Error: Missing required Bot Id. Check configs.');
        process.exit(1);
    }

    if (! fbConfigs.pageId) {
        throw Error('Error: Missing required Facebook Page Id. Check configs.');
        process.exit(1);
    }

    if (! fbConfigs.pageAccessToken) {
        throw Error('Error: Missing required Facebook Page Access Token. Check configs.');
        process.exit(1);
    }

    /*
     Set the Facebook bot parameters
     */
    botkitOptions.access_token = fbConfigs.pageAccessToken;

    /*
     Set additional required parameters if receiving webhook message events directly from Facebook
     */
    if (! bypassFacebookWebhook) {
        if (! fbConfigs.appId) {
            throw Error('Error: Missing required Facebook App Id. Check configs.');
            process.exit(1);
        }

        if (! fbConfigs.verifyToken) {
            throw Error('Error: Missing required Facebook App Verify Token. Check configs.');
            process.exit(1);
        }

        if (fbConfigs.appSecret) {
            botkitOptions.app_secret = fbConfigs.appSecret;
            botkitOptions.require_appsecret_proof = true;
        }

        botkitOptions.verify_token = fbConfigs.verifyToken;
    }

    if (botkitOptions.botkitStorage) {
        // Override Botkit's storage
        botkitOptions.storage = botkitOptions.botkitStorage;
    }

    /*
     Initialize the controller for Botkit's Facebook bot
     */
    let controller = Botkit.facebookbot(botkitOptions);

    controller.platform = customControls.config.app.platform;

    controller.factory = messageFactory;

    /*
     Load Facebook-specific event handlers
     */
    //require('../../events/facebook');


    /*
     Post bot settings to Facebook
     Only run in a production environment to prevent accidental over-writes
     */
    /*if (! isLocalEnv) {
     \        require(process.cwd() +'/bot/settings/facebook')(controller);
     }*/


    return controller;
}
