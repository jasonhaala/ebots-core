'use strict';

module.exports = {

    isNull: (value) => {
        return value === null;
    },

    isDefined: (value) => {
        return typeof value !== 'undefined';
    },

    isUndefined: (value) => {
        return typeof value === 'undefined';
    },

    isBoolean: (value) => {
        return typeof value === 'boolean';
    },

    isString: (value) => {
        return typeof value === 'string' || value instanceof String;
    },

    isObject: (value) => {
        return value && typeof value === 'object' && value instanceof Object;
    },

    isArray: (value) => {
        return value && typeof value === 'object' && value.constructor === Array;
    },

    isDate: (value) => {
        return value instanceof Date;
    },

    isSymbol: (value) => {
        return typeof value === 'symbol';
    }

}
