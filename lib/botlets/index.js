'use strict';

module.exports = {

    admin: require('./admin'),
    shop: require('./shop')

};
