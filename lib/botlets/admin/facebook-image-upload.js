'use strict';

module.exports = function() {

    controller.hears(['admin upload (.*)'], ['message_received', 'postback'], (bot, message) => {
        // match[1] is the intent word: (.*)
        // match[0] is the entire group: admin upload (.*)
        let uploadType = message.match[1] ? message.match[1].toLowerCase() : null;

        let attachment;
        switch(uploadType) {
            case 'image':
            case 'photo':
            case 'pic':
                uploadType = 'image';
                attachment = {
                    type: uploadType,
                    payload: {
                        url: "https://pbs.twimg.com/profile_images/803642201653858305/IAW1DBPw_400x400.png",
                        is_reusable: true
                    }
                };
        }



        controller.api.attachment_upload.upload(attachment, function (err, attachmentId) {
            if(err) {
                console.error(err);
                bot.reply(message, `Sorry, there was a problem while uploading your ${uploadType}.`);
            } else {

                bot.reply(message, `I uploaded your ${uploadType}. Your Facebook attachment id is: *${attachmentId}*.`);
            }
        });
    });

}
