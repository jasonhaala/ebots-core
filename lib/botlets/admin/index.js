'use strict';

module.exports = function(controller) {

    controller.hears(['admin', 'admin.index'], ['message_received', 'facebook_postback'], (bot, message) => {
        // @todo Check if user has admin privilege

        let cards = [
            {
                title: 'Broadcast Messages',
                subtitle: '',
                imageUrl: 'http://oi67.tinypic.com/2zgav6a.jpg',
                buttons: [{type: 'postback', title: 'Send Broadcast', payload: 'admin.broadcast'}]
            },
            {
                title: 'Audience',
                subtitle: '',
                imageUrl: 'http://oi67.tinypic.com/2zgav6a.jpg',
                buttons: [{type: 'postback', title: 'View Audience', payload: 'admin.audience'}]
            },
            {
                title: 'Analytics',
                subtitle: '',
                imageUrl: 'http://oi67.tinypic.com/2zgav6a.jpg',
                buttons: [{type: 'postback', title: 'View stats', payload: 'admin.analytics'}]
            },
            {
                title: 'Facebook Settings',
                subtitle: '',
                imageUrl: 'http://oi67.tinypic.com/2zgav6a.jpg',
                buttons: [{type: 'postback', title: 'View settings', payload: 'admin.facebook'}]
            }
        ];

        fbBuilder.carousel({'aspectRatio': 'horizontal'}, cards, function(error, attachment) {
            bot.reply(message, {attachment: attachment});
        });
    });


    controller.hears(['admin.analytics'], 'facebook_postback', (bot, message) => {
        bot.reply(message, 'Analytics Admin');
    });


    controller.hears(['admin.audience'], 'facebook_postback', (bot, message) => {
        bot.reply(message, 'Audience Admin');
    });


    controller.hears(['admin.broadcast'], 'facebook_postback', (bot, message) => {
        bot.reply(message, 'Broadcast Admin');
    });


    controller.hears(['admin.facebook'], 'facebook_postback', (bot, message) => {
        bot.reply(message, 'Facebook Admin');
    });

}
