'use strict';

/**
 * Webserver Up event
 */
module.exports = function(controller) {

    controller.on('server_up', (server, tunnelUrl) => {
        let notification = `Server is up!`;
        if (server.settings && server.settings.port) {
            notification += ` (port ${server.settings.port})`;
        }
        if (tunnelUrl) {
            notification += ` Public URL: ${tunnelUrl}`;
        }

        controller.log.debug(notification);
    });

}
