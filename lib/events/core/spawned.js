'use strict';

/**
 * A bot instance has been spawned
 */
module.exports = function(controller) {

    controller.on('spawned', (bot) => {
        controller.log.debug('Triggered Event: (spawned). A new bot has been spawned.');
    });

}
