'use strict';

/**
 * A trigger defined with controller.hears() was fired.
 */
module.exports = function(controller) {

    /**
     * @param {Object} [data] - The data required for processing analytics
     *      {String} [senderType] - Allowed: user or bot
     *      {String} [userId] - // a unique string identifying the user which the bot is interacting with
     *      {Number} [timestamp] - Unix Epoch timestamp with MS precision
     *      {String} [message] - The message text or payload value
     *      {Boolean} [unhandled] - Sets whether the bot heard or understood the message sent by the user
     *      {Null|String|Number} [msgId] - The unique message id
     *      {Null|String} [intent] - The agent intent
     *      {Null|String|Number} [sessionId]
     *      {Null|Boolean} [isFeedback] - Sets the message as feedback from the user, instead of a regular message
     * @param {Object} [message] - The normalized Botkit message
     */
    controller.on('send_analytics', (data, message) => {
        controller.log.debug('Triggered Event: (send_analytics)...');
        if (! data.userId) {
            controller.log.error('Analytics Requirement Error: Missing User Id.');

            return;
        }
        /*console.log('=========================')
        console.log(JSON.stringify(data));
        console.log('=========================')*/
        data.intent = data.message && data.message.intent || null;
        data.sessionId = null; // @todo Get value
        data.isFeedback = null; // @todo Get value

        /*
        Send to each of the enabled services
         */
        if (controller.analytics.chatbase) {
            sendToChatbase(controller, data, message);
        }
    });

}


function sendToChatbase(controller, data, message) {
    controller.log.debug('Sending analytics to Chatbase...');

    if (! data.timestamp) {
        controller.log.error('Chatbase Requirement Error: Missing message timestamp.');
        return;
    } else if (! data.senderType) {
        controller.log.error('Chatbase Requirement Error: Missing message sender type ("bot" or "user").');
        return;
    }
    // Get the text of the message, intent or dialog name
    let msgText = data.message;
    if (! msgText || typeof msgText === 'function' || typeof msgText === 'object') {
        // Check if the intent value was set
        let intentName = controller.getIntent();
        let dialogName = controller.getDialog();
        if (intentName) {
            msgText = intentName;
        } else if (dialogName) {
            msgText = dialogName;
        } else {
            // Message doesn't have any text or intent/dialog name to process.
            return;
        }
    }

    // Prepare the request
    let chatbase = controller.analytics.chatbase;
        chatbase.setUserId(data.userId);

    let chatbaseRequest = chatbase.newMessage();

    chatbaseRequest.setMessage(msgText);
    chatbaseRequest.setTimestamp(data.timestamp);

    if (data.senderType === 'bot') {
        chatbaseRequest.setAsTypeAgent();
    } else if (data.senderType === 'user') {
        chatbaseRequest.setAsTypeUser();

        if (data.unhandled) {
            // The bot did not hear or understand the message sent by the user
            chatbaseRequest.setAsNotHandled();
        } else {
            // The bot understood the message sent by the user
            chatbaseRequest.setAsHandled();
        }
    }

    if (data.intent) {
        // the intent of the sent message (does not have to be set for agent messages)
        chatbaseRequest.setIntent(data.intent);
    }

    if (data.msgId) {
        if (typeof data.msgId === 'object') {
            // Convert ObjectId to string
            data.msgId = data.msgId.toString();
        }
        chatbaseRequest.setMessageId(data.msgId);
    } else {
        controller.log.notice('Chatbase Warning: Missing message id.');
    }

    if (data.isFeedback) {
        // sets the message as feedback from the user
        chatbaseRequest.setAsFeedback();
    } else {
        // sets the message as a regular message -- this is the default
        chatbaseRequest.setAsNotFeedback();
    }

    if (data.sessionId) {
        chatbaseRequest.setCustomSessionId(data.sessionId);
    }

    // Send the request
    chatbaseRequest.send()
                   .then(cMsg => console.log(cMsg.getCreateResponse()))
                   .catch(err => console.error(err));

}
