'use strict';

/**
 * New Session
 */
module.exports = function(controller) {

    controller.on('fallback', (bot, message, user) => {
        controller.log.debug('Triggered Event: Unhandled message. Using fallback...');
        //controller.log.debug('==============================');
        //controller.log.debug(JSON.stringify(message));
        //controller.log.debug('==============================');

        /*
         Save received message to database
         */
        let dbData = {
            unhandled: true,
            status: 'received',
            senderType: 'user',
            message: message,
            addedAt: new Date(message.timestamp)
        }
        controller.log.debug('Saving unhandled message to database...');
        controller.ebots.db.messages.save(dbData, function(error, storedMsg) {
            if (! storedMsg._id) {
                // @todo Log/alert message not being stored
            }

            /*
             Report to analytics services
             */
            let analyticsData = {
                event: 'unhandled',
                senderType: dbData.senderType,
                userId: message.user,
                timestamp: message.timestamp.toString(),
                message: message.text || null,
                unhandled: dbData.unhandled,
                msgId: storedMsg._id || null
            }
            controller.trigger('send_analytics', [analyticsData, message]);
        });


        /*
         Handle the fallback response to the user
         */
        let customFallback = require(process.cwd() +'/bot/fallback');
        if (typeof customFallback === 'function')  {
            // Use the bot's custom fallback function to handle the response
            customFallback(controller, bot, message, user);
        } else {
            // A custom fallback function hasn't been added, use the built-in handler
            // By default, just send a response message.
            bot.reply(message, `I'm not sure how to answer that.`);
        }
    });

}
