'use strict';

const Storage = require('@google-cloud/storage');

module.exports = {

    /**
     * // Creates a Google Cloud storage client
     * @param {String|null} projectId
     */
    client: function(projectId = null) {
        let storageConfig = '';
        if (projectId) {
            storageConfig = { projectId: projectId };
        }

        return new Storage(storageConfig);
    },

    createBucket: function(projectId, bucketName, callback) {
        let client = module.exports.client(projectId);

        client.createBucket(bucketName)
        .then(() => {
            console.log(`Google Cloud Storage bucket (${bucketName}) created.`);
            callback(null, true);
        })
        .catch(err => {
            console.error('Google Cloud Storage error:', err);
            callback(err, null);
        });
    },

    upload: function(projectId, bucketName, callback) {
        let client = module.exports.client(projectId);

        // Uploads a local file to the bucket
        storage
        .bucket(bucketName)
        .upload(filename, {
            // Support for HTTP requests made with `Accept-Encoding: gzip`
            gzip: true,
            metadata: {
                // Enable long-lived HTTP caching headers
                // Use only if the contents of the file will never change
                // (If the contents will change, use cacheControl: 'no-cache')
                cacheControl: 'public, max-age=31536000',
            },
        })
        .then(() => {
            console.log(`${filename} uploaded to ${bucketName}.`);
        })
        .catch(err => {
            console.error('ERROR:', err);
        });
    }

}
