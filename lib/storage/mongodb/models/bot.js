const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const collection = 'bot';

const ModelSchema = Schema({
    title: String,
    tag: String,
    apiToken: String,
    facebook: {
        pageId: String,
        pageAccessToken: String,
        appId: String,
        appSecret: String,
        verifyToken: String,
    },
}, {
    collection: collection
});


module.exports = function(builder) {

    let model = mongoose.model(collection, ModelSchema);

    // Use the default builder methods or use a custom query function
    return {

        model,

        /*
         Required methods
         */
        get: builder.get(model),

        save: builder.save(model),

        /*
         Optional methods
         */
        //all: builder.all(model),

        //delete: builder.delete(model),

        //find: builder.find(model),

    };

};
