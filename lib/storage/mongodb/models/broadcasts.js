const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const collection = 'broadcasts';

const ModelSchema = Schema({
    title: String,
    channel: String,
    message: String,
    subscribers: [{
        userId: { type: Schema.Types.ObjectId, ref: 'users' },
        isActive: Boolean,
        sentAt: Date,
        receivedAt: Date,
        readAt: Date,
    }],
}, {
    collection: collection
});


module.exports = function(builder) {

    let model = mongoose.model(collection, ModelSchema);

    // Use the default builder methods or use a custom query function
    return {

        /*
         Required methods
         */
        //get: builder.get(model),

        save: builder.save(model),

        /*
         Optional methods
         */
        //all: builder.all(model),

        //delete: builder.delete(model),

        //find: builder.find(model),

    };

};
