const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const collection = 'users';

const ModelSchema = Schema({
    userId: { type: String, index: true, unique: true },
    isActive: {type: Boolean, default: true },
    status: String,
    source: String,
    firstName: String,
    lastName: String,
    gender: String,
    locale: String,
    picUrl: String,
    email: String,
    phone: String,
    location: {
        street: String,
        street2: String,
        city: String,
        region: String,
        postal: String,
        country: String
    },
    timezoneOffset: Number,
    currency: String,
    languages: [String],
    tags: [String],
    attributes: [{
        attribute: String,
        value: String
    }],
    sessions: [Date],
    hasOnboarded: { type: Boolean, default: false },
    getUpdates: { type: Boolean, default: false },
    pastSubscriber: { type: Boolean, default: false },
    subscriptions: [
        {
            id: Schema.Types.ObjectId,
            subscribedAt: Date
        }
    ],
    lastActiveAt: Date,
    lastUpdatedAt: { type: Date, default: Date.now },
    isAdmin: { type: Boolean, default: false },
    adminPassword: String
}, {
    collection: collection
});

ModelSchema.index({ userId: 1 });


module.exports = function(builder) {

    let schemaModel = mongoose.model(collection, ModelSchema);

    // Use the default builder methods or use a custom query function
    return {

        //model: schemaModel,

        /*
         Required methods
         */
        get: function(userId, callback) {
            return schemaModel.findOne({
                userId: userId
            }).lean().exec(callback);
        },

        save: function(data, callback) {
            data.lastUpdatedAt = new Date().toISOString();
            return schemaModel.findOneAndUpdate({
                userId: data.userId,
            }, data, {
                upsert: true,
                new: true
            }).lean().exec(callback);
        },

        /*
         Optional methods
         */
        all: builder.all(schemaModel),

        //delete: builder.delete(schemaModel),

        //find: builder.find(schemaModel),

    };

};
