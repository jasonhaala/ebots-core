const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const collection = 'messages';

const ModelSchema = Schema({
    message: Schema.Types.Mixed,
    senderType: { type: String, index: true }, // user|bot
    status: { type: String, index: true }, // received|sent||read|replied
    unhandled: { type: Boolean, default: false, index: true },
    addedAt: { type: Date, default: Date.now() }
}, {
    collection: collection
});

module.exports = function(builder) {

    let model = mongoose.model(collection, ModelSchema);

    // Use the default builder methods or use a custom query function
    return {

        /*
         Required methods
         */
        //get: builder.get(model),

        save: builder.save(model),

        /*
         Optional methods
         */
        //all: builder.all(model),

        //delete: builder.delete(model),

        //find: builder.find(model),

    };

};
