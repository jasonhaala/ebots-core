const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const collection = 'errors';

const ModelSchema = Schema({
    message: String,
    data: Schema.Types.Mixed,
    addedAt: { type: Date, default: Date.now() }
}, {
    collection: collection
});


module.exports = function(builder) {

    let model = mongoose.model(collection, ModelSchema);

    // Use the default builder methods or use a custom query function
    return {

        /*
         Required methods
         */
        //get: builder.get(model),

        save: builder.save(model),

        /*
         Optional methods
         */
        //all: builder.all(model),

        //delete: builder.delete(model),

        //find: builder.find(model),

    };

};
