const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const collection = 'scancodes';

const ModelSchema = Schema({
    platform: String,
    refTag: { type: String, unique: true },
    imageUrl: String,
    isDeleted: { type: Boolean, default: false }
}, {
    collection: collection
});

ModelSchema.index({ platform: 1, refTag: 1 }, { unique: true });

module.exports = function(builder) {

    let model = mongoose.model(collection, ModelSchema);

    // Use the default builder methods or use a custom query function
    return {

        /*
         Required methods
         */
        /*get: function(platform, refTag, callback) {
            return model.findOne({
                platform: platform,
                refTag: refTag,
            }).lean().exec(callback);
        },*/

        save: builder.save(model),

        /*
         Optional methods
         */
        /*all: function(platform, callback) {
            return model.find({
                platform: platform,
                isDeleted: false
            }).lean().exec(callback);
        },*/

        /*delete: function(platform, refTag, callback) {
            let data = {
                platform: platform,
                refTag: refTag,
                isDeleted: true
            }
            return function(data, callback) {
                if (data._id) {
                    // Update
                    return model.findOneAndUpdate({
                        _id: data._id,
                    }, data, {
                        upsert: true,
                        new: true
                    }).lean().exec(callback);
                } else {
                    // Insert new
                    return model.create(data, callback);
                }
            }
        },*/

        //find: builder.find(model),
    };

};
