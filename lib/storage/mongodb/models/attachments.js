const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const collection = 'attachments';

const ModelSchema = Schema({
    type: String,
    url: { type: String, unique: true },
    facebookAttachmentId: String,
    savedToCloud: { type: Boolean, default: false }
}, {
    collection: collection
});

module.exports = function(builder) {

    let model = mongoose.model(collection, ModelSchema);

    // Use the default builder methods or use a custom query function
    return {

        /*
         Required methods
         */
        //get: builder.get(model),

        save: builder.save(model),

        /*
         Optional methods
         */
        //all: builder.all(model),

        //delete: builder.delete(model),

        //find: builder.find(model),

    };

};
