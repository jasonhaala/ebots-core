'use strict';

const fs = require('fs');
const path = require('path');
const util = require('../../utils');
const modelMethodBuilder = require('./model-methods');

/*
 Usage examples:
 controller.ebots.db.users.save({userId: message.user, foo:'bar'}, function(err) { ... });
 controller.ebots.db.users.get(id, function(err, data) {...});
 controller.ebots.db.users.delete(id, function(err) {...});
 controller.ebots.db.users.all(function(err, data) {...});
 */

/**
 * Custom MongoDB Storage System
 *
 * @return {Object} A storage object conforming to the Botkit storage interface
 */
module.exports = function() {

    let storage = {};
    
    /*
     Create schemas from the built-in models
     */
    let newModel;
    let coreModelsDir = path.join(__dirname, './models');
    fs.readdirSync(coreModelsDir).forEach(function(file) {
        newModel = loadModel(coreModelsDir,  file);
        storage[newModel.name] = newModel.queries;
    });

    /*
     Create schemas from the custom, add-on models
     */
    let customModelsDir = path.join(process.cwd() +'/bot/storage/mongodb/models');
    fs.readdirSync(customModelsDir).forEach(function(file) {
        // Ignore any files that begin with an underscore (_)
        if (file !== 'disabled') {
            newModel = loadModel(customModelsDir, file);
            storage[newModel.name] = newModel.queries;
        }
    });

    return storage;
}

function loadModel(modelDir, modelFile) {
    let filePath = path.join(modelDir, modelFile);
    let model = require(filePath)(modelMethodBuilder);
    let modelName = path.basename(modelFile, '.js').toLowerCase();

    return {
        name: modelName,
        queries: model
    };
}
