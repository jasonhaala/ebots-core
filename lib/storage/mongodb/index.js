'use strict';

const storage = require('./storage');
const connection = require('./connection');

module.exports = {

    connection: connection,

    setup: storage,

};
