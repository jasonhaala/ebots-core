'use strict';

const mongoose = require('mongoose');
const fs = require('fs');
const tunnel = require('tunnel-ssh');
const util = require('../../utils');

module.exports = {

    /**
     * @param {Object} mongoConfig // MongoDB connection settings
     *      {String} database // (required) The database to connect to
     *      {String} host // (defaults to: localhost)
     *      {Number|String} port // (defaults to: 27017)
     *      {String} username
     *      {String} password
     *      {Number|String} uniqueLocalPort // optional Make a connection on a unique local port to prevent conflicts with other services using MongoDB
     *      {Object} ssh // Optional SSH tunnel configuration
     *          {String} host // (defaults to: localhost)
     *          {String} port // (defaults to: 27017)
     *          {String} username
     *          {String} password
     *          {String} privateKey // The path to private SSH key, if used
     * @param {Object} mongooseOptions // Optional Mongoose options
     *      {String} authSource // The database name associated with the authenticated user credentials (defaults to: admin)
     *      {Boolean} autoIndex // Don't build indexes (index builds can cause performance degradation) (defaults to: false)
     *      {Boolean} autoReconnect // (defaults to: true)
     *      {Number} rreconnectTries // (defaults to: 10)
     *      {Number} reconnectInterval // Reconnect every <#> ms (defaults to: 500)
     *      {Number} rpoolSize // (defaults to: 5, // # of socket connections to keep open
     *      {Number} rbufferMaxEntries // (defaults to: 0)
     *      {Boolean} keepAlive // (defaults to: 120)
     *      {Boolean} useNewUrlParser // (defaults to: true)
     */
    connect: function(mongoConfig, mongooseOptions, callback) {
        /*
         Check parameters
         */
        if (! mongoConfig.db) {

            return callback(`Database name is required.`);

        }
        if (! mongoConfig.host) {
            mongoConfig.host = 'localhost';
        }
        if (! mongoConfig.port) {
            mongoConfig.port = 27017;
        }
        if (! mongoConfig.tables) {
            mongoConfig.tables = [];
        } else if (util.isString(mongoConfig.tables)) {
            mongoConfig.tables = [mongoConfig.tables];
        }

        /*
         Check SSH configs
         */
        const sshHost = mongoConfig.ssh.host;
        const sshPort = mongoConfig.ssh.port;
        const sshUser = mongoConfig.ssh.user;
        const sshPassword = mongoConfig.ssh.pass;
        const sshPrivateKeyPath = mongoConfig.ssh.privateKey;

        if (! (sshHost && sshPort && sshUser && (sshPassword || sshPrivateKeyPath))) {
            /**
             * Tunnel-less connection
             */
            console.log('Creating a tunnel-less MongoDB connection...');
            // Add security warnings for remote connections
            if (! (mongoConfig.host === '127.0.0.1' || mongoConfig.host === 'localhost')) {
                console.log(`Security Warning: Remote MongoDB connection is not using a secure tunnel. Consider using SSH keys.`);

                if (! (mongoConfig.username && mongoConfig.password)) {
                    console.log('Security Warning: MongoDB is not protected by a username or password.');
                }
            }

            /*
             Connect to MongoDB
             */
            return connection(mongoConfig, mongooseOptions, callback);
        } else {
            /**
             * SSH Tunnel connection
             */
            console.log('Creating a MongoDB connection using a secure SSH tunnel...');
            // Use unique port to prevent conflicts with other server apps using MongoDB
            if (! mongoConfig.uniqueLocalPort) {
                mongoConfig.uniqueLocalPort = Math.floor(Math.random() * 50000) + 27018;
            }

            const tunnelConfig = {
                host: sshHost,
                port: sshPort,
                username: sshUser,
                dstHost: mongoConfig.host,
                dstPort: mongoConfig.port,
                localPort: mongoConfig.uniqueLocalPort,
                localHost: '127.0.0.1',
            };
            if (sshPassword) {
                tunnelConfig.password = sshPassword;
            } else {
                if (! sshPrivateKeyPath) {

                    return callback(`Storage is missing the required MongoDB SSH private key.`);
                }
                tunnelConfig.privateKey = fs.readFileSync(sshPrivateKeyPath);
            }

            tunnel(tunnelConfig, function (error, server) {
                if (error) {

                    return callback(error);
                }

                /*
                 Connect to MongoDB
                 */
                return connection(mongoConfig, mongooseOptions, callback);
            });
        }
    }

}


/**
 * Connect to MongoDB
 *
 * @param {Object} mongoConfig
 * @param {Object} mongooseOptions
 * @param {Function} callback
 */
function connection(mongoConfig, mongooseOptions, callback) {

    /*
     Configure Mongoose
     */
    if (! util.isObject(mongooseOptions)) {
        mongooseOptions = {};
    }
    let mongooseConfig = {
        authSource: mongooseOptions.authSource || 'admin',
        dbName: mongoConfig.db,
        autoIndex: mongooseOptions.autoIndex || false,
        autoReconnect: mongooseOptions.autoReconnect || true,
        reconnectTries: mongooseOptions.reconnectTries || 10,
        reconnectInterval: mongooseOptions.reconnectInterval || 500,
        poolSize: mongooseOptions.poolSize || 5,
        bufferMaxEntries: mongooseOptions.bufferMaxEntries || 0,
        keepAlive: mongooseOptions.keepAlive || 120,
        useNewUrlParser: mongooseOptions.useNewUrlParser || true
    };

    if (mongoConfig.user) {
        mongooseConfig.user = mongoConfig.user;
    }
    if (mongoConfig.pass) {
        mongooseConfig.pass = mongoConfig.pass;
    } else {
        if (! mongoConfig.ssh.privateKey) {
            console.log('Security Warning: MongoDB connection is not using a password or private key.');
        }
    }

    /*
     Create the Mongoose connection
     */
    let connectionPort = mongoConfig.uniqueLocalPort || mongoConfig.port;
    let connectionString =`mongodb://${mongoConfig.host}:${connectionPort}`;

    mongoose.connect(connectionString, mongooseConfig);

    let db = mongoose.connection;

    /*
     Connection Events
     */
    // When successfully connected
    db.on('connected', function () {
        console.log('Successful MongoDB connection made!');

        return callback(null, true);
    });

    // If the connection throws an error
    db.on('error', function (error) {
        console.log('MongoDB connection error: ' + error);

        return callback(error);
    });

}
