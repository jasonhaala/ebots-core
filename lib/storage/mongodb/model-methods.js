'use strict';

module.exports = {

    all: function(model) {
        return function(callback) {
            model.find({}).lean().exec(callback);
        }
    },

    delete: function(model) {
        return function(id, callback) {
            return model.findOneAndDelete()({
                _id: id
            }, callback);
        }
    },

    find: function(model) {
        return function(data, callback, options) {
            return model.find(data, null, options).lean().exec(callback);
        }
    },

    get: function(model) {
        return function(id, callback) {
            return model.findOne({
                _id: id
            }).lean().exec(callback);
        }
    },

    save: function(model) {
        return function(data, callback) {
            if (data._id) {
                // Update
                return model.findOneAndUpdate({
                    _id: data._id,
                }, data, {
                    upsert: true,
                    new: true
                }).lean().exec(callback);
            } else {
                // Insert new
                return model.create(data, callback);
            }
        }
    }

}
