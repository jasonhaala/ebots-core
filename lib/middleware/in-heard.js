'use strict';

/**
 * Heard Middleware  (incoming)
 *
 * The heard middleware endpoint occurs after a message has matched a trigger pattern,
 * and is about to be handled. It works just like the receive endpoint, but instead of firing for
 * every incoming message, it will only fire for messages that the is explicitly listening for.
 *
 * This makes the heard endpoint useful for firing expensive operations,
 * such as database lookups or calls to external APIs tat take a long time, require a lot of processing,
 * or actually cost money to use. However, it makes it less useful for use with NLP tools,
 * since the pattern matching have already occurred.
 *
 * Note that the heard middleware fires only for messages that match handlers set up with controller.hears(),
 * and does not fire with handlers configured with the controller.on() method.
 */
module.exports = function(controller) {

    controller.middleware.heard.use(function(bot, message, next) {

        controller.log.debug('Middleware (heard): Message Heard...');

        let dbData = {
            unhandled: false,
            status: 'received',
            senderType: 'user',
            message: message,
            addedAt: new Date(message.timestamp)
        }
        controller.ebots.db.messages.save(dbData, function(error, storedMsg) {
            if (error) {
                controller.log.error(error);
                // @todo Log/alert message not being stored
            }

            /*
             Report to analytics services
             */
            let analyticsData = {
                event: 'heard',
                senderType: dbData.senderType,
                userId: message.user,
                timestamp: message.timestamp.toString(),
                message: message.text || null,
                unhandled: dbData.unhandled,
                msgId: storedMsg._id || null
            }
            controller.trigger('send_analytics', [analyticsData, message]);
        });

        next();
    });

}
