'use strict';

/**
 * Normalize Middleware (incoming)
 *
 * Normalization is the second phase of the message pipeline.
 * This middleware is used to copy, rename, or transform raw message fields
 * into a usable object while leaving everything else alone.
 *
 * After passing through the normalize phase, the message object is expected to have the following fields:
 * type:
 *      will contain either the raw value of the incoming type field specified by the platform,
 *      OR message_received which is the default message type defined by Botkit.
 * user
 *      will contain the unique id of the sending user
 * channel
 *      will include the unique id of the channel in which the message was sent
 * text
 *      will contain the text, if any, of the message.
 *
 * The entire original unmodified message object will still be available as message.raw_message.
 */
module.exports = function(controller) {

    controller.middleware.normalize.use(function(bot, message, next) {

        //console.log('Middleware: Normalize...');

        /*
         Normalize Facebook quick replies into a "facebook_postback" message type
         */
        if (message.quick_reply) {
            message.postback = {
                payload: message.quick_reply.payload,
                title: message.text
            }
            message.payload = message.quick_reply.payload;
            message.text = message.quick_reply.payload;
            message.type = 'facebook_postback';
        }

        next();
    });

}
