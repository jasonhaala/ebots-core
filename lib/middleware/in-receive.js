'use strict';

/**
 * Recieve Middleware (incoming)
 *
 * Receive is the final step in the incoming message pipeline before the message actually reaches the bot's internal
 * logic.
 *
 * By the time a message hits the receive stage, it is in its final form,
 * and is ready to be processed by a Botkit event handler.
 *
 * This middleware endpoint occurs just before a message is evaluated for trigger matches,
 * and before any user-defined handler runs.
 *
 * It will fire for every incoming message, regardless of whether or not it matches a trigger
 * or if any event handlers are registered to receive it.
 */
module.exports = function(controller) {

    controller.middleware.receive.use(function(bot, message, next) {
        controller.log.debug('==============================');
        controller.log.debug(`Middleware: (receive) Received...`);
        //controller.log.debug(JSON.stringify(message));
        controller.log.debug('==============================');

        /*
        Get and Set user profile in session
         */
        controller.getProfile(message.sender.id, function(profile, isNewUser) {
            if (profile) {
                bot.userProfile = profile;
            } else {
                bot.userProfile = {};
            }

            next();
        });
    });

}
