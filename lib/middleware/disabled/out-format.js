'use strict';

/**
 * Format Middleware (outgoing)
 *
 * This middleware happens immediately before a message is delivered to the platform API.
 *
 * Each platform as its own special format for incoming message objects.
 * This middleware should exclusively be used for constructing the final API parameters required
 * for delivering the message. The message object that emerges from this function
 * is intended only for use with the messaging service API.
 *
 * After being formatted, the resulting platform_message is passed into the platform-specific bot.send() function,
 * which is responsible for the final delivery of the message the appropriate external API endpoint.
 * This allows the bot.send() function to be designed to accept only pre-formatted messages.
 *
 * Unlike all the other pipeline endpoints, this function does NOT modify the original message object.
 * In fact, the final object is constructed by the middleware in the platform_message parameter,
 * allowing the original message to pass through unmolested.
 */

module.exports = function(controller) {

    controller.middleware.send.use(function(bot, message, next) {

        console.log('Middleware: Format...');

        next();

    });

}
