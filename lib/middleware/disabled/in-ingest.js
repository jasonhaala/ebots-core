'use strict';

/**
 * Ingest Middleware (incoming)
 *
 * Ingestion into Botkit is the first step in the message pipeline.
 *
 * Message objects that pass through the ingest phase will have 2 additional fields:
 * message.raw_message:
 *      Contains the unmodified content of the incoming message payload as received from the messaging service
 * message._pipeline:
 *      An object that tracks a message's progress through the pipeline.
 *      The subfield message._pipeline.stage will contain the name of the current pipeline step.
 */
module.exports = function(controller) {

    controller.middleware.ingest.use(function(bot, message, res, next) {

        console.log('Middleware: Ingest...');
        // define action
        // perhaps set an http status header
        // res.status(200);
        // you can even send an http response
        // res.send('OK');

        // you can access message.raw_message here

        // call next to proceed
        next();

    });

}
