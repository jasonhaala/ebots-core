'use strict';

/**
 * Categorize Middleware  (incoming)
 *
 * Categorization is the third phase of the message pipeline.
 *
 * After passing through the categorize phase,
 * the message object's type field should represent a the final event type that will be handled by Botkit.
 *
 * The most obvious example of a categorization action is identifying and transforming a message from a
 * generic message_received event into more narrowly defined direct_mention, direct_mention, mention or ambient message event.
 */
module.exports = function(controller) {

    controller.middleware.categorize.use(function(bot, message, next) {

        console.log('Middleware: Categorize...');
        // messages in Slack that are sent in a 1:1 channel
        // can be identified by the first letter of the channel ID
        // if it is "D", this is a direct_message!
        /*if (message.type == 'message_received') {
            if (message.channel[0] == 'D') {
                message.type = 'direct_message';
            }
        }*/

        // call next to proceed
        next();

    });

}
