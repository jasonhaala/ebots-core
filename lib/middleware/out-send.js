'use strict';

/**
 * Send Middleware
 *
 * When a message is sent with bot.send() or bot.reply(), the outgoing message is first sent through the send middleware.
 */

module.exports = function(controller) {

    controller.middleware.send.use(function(bot, message, next) {

        if (message.sender_action) {
            // Don't process any typing actions
            next();
            return;
        }

        controller.log.debug('==============================');
        controller.log.debug('Middleware (send): Send...');
        //controller.log.debug(JSON.stringify(message));
        controller.log.debug('==============================');

        /*
         Save received message to database
         */
        let msgDate = new Date();
        let msgTimestamp = msgDate.getTime();

        let dbData = {
            unhandled: false,
            status: 'sent',
            senderType: 'bot',
            message: message,
            addedAt: msgDate
        }
        controller.ebots.db.messages.save(dbData, function(error, storedMsg) {
            if (error) {
                controller.log.error(error);
                // @todo Log/alert message not being stored
            }

            /*
             Report to analytics services
             */
            let analyticsData = {
                event: 'send',
                senderType: dbData.senderType,
                userId: message.to,
                timestamp: msgTimestamp.toString(),
                message: message.text || null,
                unhandled: dbData.unhandled,
                msgId: storedMsg._id || null
            }

            controller.trigger('send_analytics', [analyticsData, message]);

            next();
        });

    });

}
