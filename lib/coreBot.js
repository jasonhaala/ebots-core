'use strict';

/*
 Core Modules
 */
const path = require('path');
const fs = require('fs');
//const cacheManager = require('cache-manager');
const Redis = require('ioredis');

/*
 Internal Modules
 */
const mongoConnection = require('./storage/mongodb/connection');
const mongoStorage = require('./storage/mongodb/storage');
const userProfile = require('./userProfile');

/*
 Placeholder variable for the module that will build the platform-specific bot controller.
 */
let platformBot;

module.exports = function(config, callback) {

    const isProduction = config.app.env === 'production';
    const botId = config.app.botId;
    let platform = config.app.platform
    let errorMsg;

    if (! botId) {
        throw new Error(`Missing the required bot id. Check the config.app.botId setting.`);
        process.exit();
    }

    /*
     Check if the required configurations are set
     */
    if (! platform) {
        throw new Error(`Missing the required messaging platform. Check the config.app.platform setting.`);
        process.exit();
    } else {
        platform = platform.toLowerCase();

        // Ensure that a valid platform value is passed
        switch (platform) {
            case 'facebook':
                platformBot = require('./platforms/'+platform+'/bot');
                break;
            case 'slack':
            case 'web':
            case 'twilioSMS':
            case 'twilioIPM':
            case 'microsoftTeams':
            case 'webexTeams':
            case 'ciscoJabber':
                throw new Error(`${platform} is currently not a supported platform.`);
                process.exit();
                return;
            default:
                throw new Error(`${platform} is not a valid bot platform. Check the config.app.platform setting.`);
                process.exit();
                return;
        }
    }

    /*
    Check configs for the chosen platform
     */
    if (! config[platform]) {
        throw new Error(`Missing the required ${platform} config file.`);
        process.exit();
    } else if (config.nlp.dialogflow.enabled) {
        // Check of the required Dialogflow keyfile exists
        let dialogflowKeyfile = path.join(process.cwd(), './keys/'+config.nlp.dialogflow.keyFile);
        try {
            fs.accessSync(dialogflowKeyfile);

            // Replace the filename with the full path
            config.nlp.dialogflow.keyFile = dialogflowKeyfile;
        } catch (error) {
            errorMsg = `Missing required Dialogflow keyfile. Check configs.`
            if (isProduction) {
                throw new Error(errorMsg);
                process.exit();
            } else {
                console.log(errorMsg);
            }
        }
    }


    /**
     * MongoDB Connection
     */
    let mongoConfig = config.storage.mongodb;
    if (mongoConfig.ssh.privateKey) {
        mongoConfig.ssh.privateKey = path.normalize(process.cwd() +'/keys/'+ mongoConfig.ssh.privateKey);
    }

    let mongooseOptions = {};
    mongoConnection.connect(mongoConfig, mongooseOptions, function(error, success) {
        if (error) {

            throw new Error(error);
            process.exit();
        }

        // @todo Get bot data from database instead of being stored in config file
        let customControls = {
            bot: {
                id: botId
            },
            channel: {
                name: platform,
            },
            config: {
                app: config.app,
                cache: config.cache,
                analytics: config.analytics,
                nlp: config.nlp,
                [platform]: config[platform]
            },
            db: {}, // Custom storage functions will be set here
        };


        /*
         Build the database models
         */
        let botkitStorage;
        let customStorage = mongoStorage();
        if (
            customStorage.bot.get &&
            customStorage.bot.save &&

            customStorage.users.get &&
            customStorage.users.save
        ) {
            console.log('*** Custom storage system enabled. ***');
            botkitStorage = {
                teams: {
                    get: function(){},
                    save: function(){}
                },
                users: {
                    get: function(){},
                    save: function(){}
                },
                channels: {
                    get: function(){},
                    save: function(){}
                },
            }
            if (config.storage.enableMessageLogs && customStorage.messages && customStorage.messages.get && customStorage.messages.save) {
                console.log('*** Message logging is: ENABLED ***');
            } else {
                //customStorage.messages.save = function(){};
                console.log('*** Message logging is: DISABLED ***');
            }

            customControls.db = customStorage;
        } else {
            throw new Error('Custom storage is missing required methods. Check models.');
            process.exit();
        }


        /**
         * Create the controller. This controls all instances of the bot.
         *
         * Facebook-specific event handlers will be loaded during this process
         */

            // Use a separate app to route message events received from Facebook.
        const bypassFacebookWebhook = true;

        let controller = platformBot(customControls, {
            debug: config.app.debug || false, // Enable debug logging
            stats_optout: true, // Enable/disable Botkit Studio stats
            studio_token: null, // An API token from Botkit Studio
            replyWithTyping: false, // Send typing indicators automatically
            typingDelayFactor: false, // Adjust the speed of the typing delay,
            require_delivery: false, // Facebook delivery confirmation,
            botkitStorage: botkitStorage // Override Botkit's storage
            //logger: applog
        }, bypassFacebookWebhook);


        /**
         * CUSTOM CONTROLS
         *
         * Embed our custom controls into the Botkit controller
         */
        controller.ebots = customControls;
        // This function will handle requesting,storing and returning the profile.
        controller.getProfile = userProfile(controller);


        if (controller.ebots.db) {
            // Override Botkit's built-in storage to disable warning messages
            if (botkitStorage) {
                controller.storage = botkitStorage;
            }
        }

        /**
         * Caching System
         */
        if (config.cache.default === 'redis') {
            const cache = new Redis({
                port: config.cache.redis.port,
                host: config.cache.redis.host,
                password: config.cache.redis.pass,
                db: 0
            });
            controller.cache = cache;
        }


        /**
         * Session
         */
        let session = [];


        /*
         Analytics
         */
        controller.analytics = {};

        if (config.analytics.chatbase.enabled && config.analytics.chatbase.apiKey) {
            console.log('Chatbase analytics is enabled.')
            let chatbase = require('@google/chatbase')
                .setApiKey(config.analytics.chatbase.apiKey)
                .setPlatform(platform)
                .setVersion('1.0')
                .setClientTimeout(5000);

            controller.analytics.chatbase = chatbase;
        }

        /*
        Set the production environment detection
         */
        controller.isProduction = config.app.env.toLowerCase() === 'production' ? true : false;

        /*
         Setup the message dialog and intent values
         */
        controller.setDialog = function(dialogName) {
            controller.botDialog = 'dialog.'+dialogName;
        }
        controller.getDialog = function() {
            return controller.botDialog || null;
        }
        controller.setIntent = function(intentName) {
            controller.botIntent = 'intent.'+intentName;
        }
        controller.getIntent = function() {
            return controller.botIntent || null;
        }
        

        callback(null, controller);
    });

}
